## ownCloud 9 container with goodies

This is my spin on a flexible owncloud container that is configured by cloud-init and is made to run behind a reverse proxy.

## Features:

* ownCloud 9.0.2
* apache2
* mod_php
* System cron
* Samaba Support
* LDAP/AD auth support
* MySQL and SQLite support
* ocDownloader w/ aria2c and youtubedl enabled
* /var/www/owncloud/data exposed as mount point

## Preinstalled Apps:

* ocDownloader with YouTube and Aria2 support
* passwords
* notes
* tasks
* calender
* contacts
* mail
* bookmarks
* audios
* files_mv
* files_markdown

## Examples:

### Simple Example:

```
$ docker run -tdi --name=owncloud \
           -p 80:80 -p 443:443 \
           --cap-add SYS_ADMIN \
           -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
           -v /path/to/data:/var/www/owncloud/data \
           geiseri/owncloud
$ docker exec -ti owncloud su www-data -s /bin/bash -c \
           'php /var/www/owncloud/occ maintenance:install -n \
               --database sqlite \
               --admin-user admin \
               --admin-pass admin \
           '
```

*NOTE:* The shared data directory must be writable by user "33" (www-data) and group "33" (www-data).

### Custom Configuration Example

```
$ docker run -tdi --name=owncloud \
           -p 80:80 -p 443:443 \
           --cap-add SYS_ADMIN \
           -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
           -v /path/to/data:/var/www/owncloud/data \
           -v /path/to/config.php:/var/www/owncloud/config/config.php \
           geiseri/owncloud
```

For more information on the configuration file format see:

* http://doc.owncloud.com/server/9.0/admin_manual/configuration_server/reverse_proxy_configuration.html
* http://doc.owncloud.com/server/9.0/admin_manual/configuration_server/logging_configuration.html
* http://github.com/owncloud/core/blob/v9.0.2/config/config.sample.php

### Mysql Example:

```
$ docker run -tdi --name=oc-mysql \
           -v /path/to/data/mysql:/var/lib/mysql \
           -e MYSQL_ROOT_PASSWORD="mysecret" \
           -e MYSQL_DATABASE="owncloud" \
           -e MYSQL_USER="oc_user" \
           -e MYSQL_PASSWORD="oc_secret" \
           mysql:5.5
$ docker run -tdi --name=owncloud \
           -p 80:80 -p 443:443 \
           --cap-add SYS_ADMIN \
           -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
           -v /path/to/data:/var/www/owncloud/data \
           --link oc-mysql:mysql_server \
           geiseri/owncloud
$ docker exec -ti owncloud su www-data -s /bin/bash -c \
          'php /var/www/owncloud/occ maintenance:install -n \
              --database mysql \
              --database-name $MYSQL_SERVER_ENV_MYSQL_DATABASE \
              --database-host $MYSQL_SERVER_PORT_3306_TCP_ADDR \
              --database-user $MYSQL_SERVER_ENV_MYSQL_USER \
              --database-pass $MYSQL_SERVER_ENV_MYSQL_PASSWORD \
              --admin-user admin --admin-pass admin'
```
